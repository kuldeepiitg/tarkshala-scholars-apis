Ledger
=====

* Keep a db table for all debits and credits for every student

    invoice-id(p-key)   | date(s-key)| scholar-id     | amount| type(service/debit/credit) | paid-by    | paid-to | mode-of-payment | transaction-number
    -------             | ---------- |    ------      |---    | -----------------           | ----       | -----   | --------------- | -------
    1                   | 1579919548 |vishnu@gmail.com|25000  | S                           |            |         | 
    2                   | 1579919577 |vishnu@gmail.com|25000  | C                           |aditya sahal| Kuldeep | Paytm           |  xgu5wgl

* Make **invoice-id** as primary key, it should be a number auto incremented by engine.
* Keep **date** as sort-key, so that database give us sorted output.
* **Type** tells if the transaction is debit or credit, a third type service tells that it is service charge.
* **Paid-By**  is the name of person who actually paid.
* **Paid-to** is the name of the person who received the amount.
* **transaction-number** for non-cash payments.

