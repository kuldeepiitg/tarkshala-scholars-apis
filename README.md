Layout
=====

The purpose of this project is to provide APIs for tools for scholars. It will be a simple portal where a parent can
have a watch on **fee dues, attendance and test performance**. First thing that is needed to be implemented will be fee page.

Flow for that will be as following:
  * A user will open in his browser http://scholars.tarkshala.com/login
     ![Alt text](wiki-resources/login.png)
  * User is shown his home page, which have a menu bar,
    * first tab of the menu is **Fee/Dues**
    * second tab of the menu is **Attendance**
    * third tab of the menu is **performance**
        * On pressing a tab will populate corresponding view
        * Fee page will have following informations in tabular form
            or equivalent.
            * Total fee
            * Scholarship
            * Installment paid [amount, date, remark]
            * Amount remaining


APIs
==============

Login
----------
url: http://scholars.tarkshala.com:8080/authentication/login

Method: POST

Request body(Json):

    {
        "username" : "kuldeep5534@gmail.com",
        "secret" : "mangalam"
    }

Response body(Success):

     {
         "uid": null,
         "status": 1,
         "message": "User logged in successfully",
         "data": {
             "uid": "kuldeep5534@gmail.com",
             "name": "Vishnu Hari Sahal",
             "parentName": "Aditya Sahal",
             "email": "kuldeep5534@gmail.com",
             "phones": {
                 "papa": "8209905583",
                 "mom": "9079945235"
             },
             "standard": "IX",
             "school": "MPS, Bhiwadi",
             "address": "Ashiana Aangan",
             "joiningDate": 1578826543983,
             "subjects": [
                 "Maths",
                 "Science"
             ],
             "remark": "Good student, potential candidate for IIT JEE Foundation course",
             "foundUsOn": "Hunting"
         }
     }

Request body(Json):

    {
        "username" : "wrongUsername@gmail.com",
        "secret" : "mangalam"
    }

Response body(Failure):

     {
         "uid": null,
         "status": 0,
         "message": "User is not found in the system",
         "data": null
     }

Request body(Json):

    {
        "username" : "kuldeep5534@gmail.com",
        "secret" : "wrongPassword"
    }

Response body(Failure):

     {
         "uid": null,
         "status": 0,
         "message": "Wrong username or password",
         "data": null
     }

Response cookie:

    SESSION_ID=G8ZspY7S,
    USERNAME=kuldeep5534@gmail.com

Usage: Make a post request at given url with request object. Use response body to log message client side
and cookie to start session. Save the session details. This session will be sent to server for authentication of next APIs.

Invoice
----------
url: http://scholars.tarkshala.com:8080/account/invoice

Method: POST

Request body(text):
    
    kuldeep5534@gmail.com
    
Request header:
    
    SESSION_ID=G8ZspY7S,
    USERNAME=kuldeep5534@gmail.com
    
Response body(json):

    {
        "uid": null,
        "transactions": [
            {
                "uid": "02f45eb6b5ff4026b9a23760c2d61944",
                "timestamp": 1580180549244,
                "scholarUid": "kuldeep5534@gmail.com",
                "amount": 150,
                "transactionType": "FINE",
                "paidBy": null,
                "paidTo": null,
                "modeOfPayment": null,
                "transactionId": null
            },
            {
                "uid": "dbce7bfec3ed47cea755722e4d677332",
                "timestamp": 1580180559154,
                "scholarUid": "kuldeep5534@gmail.com",
                "amount": 100,
                "transactionType": "FINE",
                "paidBy": "Vishnu",
                "paidTo": "Kuldeep",
                "modeOfPayment": "Paytm",
                "transactionId": "933ffb68-72be-4146-8114-b47f844a6eb4"
            },
            {
                "uid": "5cc13602fc5340df868f247edee60fb4",
                "timestamp": 1580180585526,
                "scholarUid": "kuldeep5534@gmail.com",
                "amount": 25000,
                "transactionType": "CREDIT",
                "paidBy": "Vishnu",
                "paidTo": "Kuldeep",
                "modeOfPayment": "Paytm",
                "transactionId": "933ffb68-72be-4146-8114-b47f844a6eb5"
            }
        ]
    }
    
Role based Access
========

APIs should be accessible depending on the user role.

Proposed user role : Student, Administrator

##Solution
We will store role with each user while registering a user.
Whenever a user tries to hit an API, roles he is assigned should be fetched from DB. 
These roles should be used to check if user have access to the given API.
    
 
================
