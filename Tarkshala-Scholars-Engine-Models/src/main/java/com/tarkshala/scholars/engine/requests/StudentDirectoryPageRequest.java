package com.tarkshala.scholars.engine.requests;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDirectoryPageRequest {

    private int count;

    private String lastEvaluatedKey;

    private List<Filter> filters;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Filter {

        private String attributeName;

        private String attributeValue;

        private Operation operation;

        private AttributeType attributeType;

        public enum Operation {
            EQ, GE, LE, GT, LT, CONTAINS, STARTS_WITH
        }

        public enum AttributeType {
            STRING, NUMBER
        }
    }
}
