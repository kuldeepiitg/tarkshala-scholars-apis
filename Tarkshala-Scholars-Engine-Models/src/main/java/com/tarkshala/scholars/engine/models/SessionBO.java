package com.tarkshala.scholars.engine.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionBO extends EntityBO {

    private String username;

    private String sessionId;
}
