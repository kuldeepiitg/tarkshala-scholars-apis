package com.tarkshala.scholars.engine.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionBO extends EntityBO {

    private String scholarUid;

    private int amount;

    private String transactionType;

    private String paidBy;

    private String paidTo;

    private String modeOfPayment;

    private String transactionId;

    private long dueDate;
}
