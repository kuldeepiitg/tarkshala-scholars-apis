package com.tarkshala.scholars.engine.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class CredentialBO extends EntityBO {

    /**
     * Email id is acting as username
     */
    private String username;

    /**
     * Password or its hash
     */
    private String secret;
}
