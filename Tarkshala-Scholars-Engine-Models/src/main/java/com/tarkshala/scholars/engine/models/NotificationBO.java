package com.tarkshala.scholars.engine.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationBO extends EntityBO {

    private Type type;

    private String subject;

    private String emailTemplate;

    private String textTemplate;

    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    public enum Type implements Serializable {
        USER_ACCOUNT_ACTIVATION("USER ACCOUNT ACTIVATION"),
        USER_ACCOUNT_DEACTIVATION("USER ACCOUNT DEACTIVATION"),
        USER_ACCOUNT_REACTIVATION("USER ACCOUNT REACTIVATION"),
        USER_PASSWORD_RESET("USER PASSWORD RESET"),
        FINANCE_ACCOUNT_DEBIT("FINANCE ACCOUNT DEBIT"),
        FINANCE_ACCOUNT_CREDIT("FINANCE ACCOUNT CREDIT"),
        FINANCE_ACCOUNT_FINE("FINANCE ACCOUNT FINE"),
        FINANCE_ACCOUNT_SERVICE("FINANCE ACCOUNT SERVICE"),
        FINANCE_FEE_DODGER("FINANCE FEE DODGER");

        private String name;
    }
}
