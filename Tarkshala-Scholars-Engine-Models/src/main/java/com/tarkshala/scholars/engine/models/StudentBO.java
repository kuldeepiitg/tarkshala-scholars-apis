package com.tarkshala.scholars.engine.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentBO extends UserBO {

    private String email;

    private String parentName;

    private Map<String, String> phones;

    private String standard;

    private String school;

    private String address;

    private Date joiningDate;

    private Set<String> subjects;

    private String remark;

    private String foundUsOn;
}
