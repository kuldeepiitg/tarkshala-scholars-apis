package com.tarkshala.scholars.engine.webservices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.util.Base64;
import java.util.Date;
import java.util.Set;

public class AuthenticationServiceHandlerTest {

    @Test
    public void greet() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(Set.of("S", "M")));
    }

    @Test
    public void digest() {
        System.out.println(Base64.getEncoder().encodeToString(DigestUtils.sha256("hello" + "world")));
    }

    @Test
    public void registerAdministrator() {
        Date date = new Date();
        System.out.println(date.getTime());
    }
}
