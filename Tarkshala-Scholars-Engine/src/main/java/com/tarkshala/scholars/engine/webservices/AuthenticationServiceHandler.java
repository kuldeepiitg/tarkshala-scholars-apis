package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.crypto.PasswordHasher;
import com.tarkshala.scholars.engine.dao.EntityDAO;
import com.tarkshala.scholars.engine.dao.model.NotificationDO;
import com.tarkshala.scholars.engine.dao.model.PasswordResetDO;
import com.tarkshala.scholars.engine.dao.model.StudentDO;
import com.tarkshala.scholars.engine.dao.model.UserDO;
import com.tarkshala.scholars.engine.exception.*;
import com.tarkshala.scholars.engine.models.*;
import com.tarkshala.scholars.engine.notification.EmailNotifier;
import com.tarkshala.scholars.engine.security.PasswordResetLinkGenerator;
import com.tarkshala.scholars.engine.security.Role;
import com.tarkshala.scholars.engine.security.SessionManager;
import com.tarkshala.scholars.engine.webservices.converters.Translator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.Optional;

@Component
@Slf4j
public class AuthenticationServiceHandler {

    @Autowired
    private EntityDAO<StudentDO> studentDAO;

    @Autowired
    private EntityDAO<PasswordResetDO> passwordResetDAO;

    @Autowired
    private EntityDAO<UserDO> userDAO;

    @Autowired
    private EntityDAO<NotificationDO> notificationDAO;

    @Autowired
    private PasswordHasher passwordHasher;

    @Autowired
    private Translator<StudentDO, StudentBO> studentTranslator;

    @Autowired
    private Translator<UserDO, UserBO> userTranslator;

    @Autowired
    private PasswordResetLinkGenerator passwordResetLinkGenerator;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private EmailNotifier emailNotifier;

    public Response greet(){
        log.debug("Welcome to Tarkshala Scholar APIs");
        return Response.ok("Hello World !! - Jersey 2").build();
    }

    /**
     * Get all available roles.
     *
     * @return OK(200) array of all roles
     */
    public Response getAvailableUserRoles() {
        log.debug("Getting all possible user roles");
        return Response.ok(Role.getRoles()).build();
    }

    /**
     * Register a new user
     *
     * @return ok(200) status
     * @param student
     */
    public Response registerScholar(StudentBO student) {

        log.debug("Registering new student with name {}", student.getName());
        StudentDO studentDO = studentTranslator.translate(student);
        Optional<StudentDO> existingStudent = studentDAO.get(student.getUid());
        if (existingStudent.isPresent()) {
            log.error("Account with the email id {} already exists", existingStudent.get().getEmail());
            throw new UserAlreadyRegisteredException("User already exists with given email id");
        }
        studentDAO.save(studentDO);

        UserBO userBO = UserBO.builder()
                .uid(student.getUid())
                .email(student.getEmail())
                .name(student.getName())
                .roles(Collections.singletonList(Role.STUDENT))
                .build();
        createUser(userBO);

        passwordResetLinkGenerator.generateLink(student.getEmail());

        return Response.ok("Student registered successfully").build();
    }

    /**
     * Create new user without password. Password should be generated later.
     */
    private void createUser(UserBO userBO) {

        log.debug("Creating new user with email id {}", userBO.getEmail());
        Optional<UserDO> existingUser = userDAO.get(userBO.getUid());
        if (existingUser.isPresent()) {
            log.error("Account exists for some user with same email id {}", userBO.getEmail());
            throw new UserAlreadyRegisteredException("User already exists with given email id.");
        }

        String salt = RandomStringUtils.randomAlphanumeric(8);
        UserDO user = UserDO.builder()
                .uid(userBO.getUid())
                .email(userBO.getEmail())
                .name(userBO.getName())
                .roles(userBO.getRoles())
                .salt(salt)
                .status(UserDO.Status.ACTIVE)
                .build();

        userDAO.save(user);
    }

    /**
     * forgot password API
     * @param username unique username
     *
     * @return
     */
    public Response forgotPassword(String username) {
        log.debug("Forgot password invoked for user {}", username);
        passwordResetLinkGenerator.generateLink(username);
        return Response.ok("Sent an email to reset password").build();
    }

    /**
     * Reset password API
     *
     * @param resetPasswordBO
     * @return
     */
    public Response resetPassword(ResetPasswordBO resetPasswordBO) {

        log.debug("Resetting password");
        Optional<PasswordResetDO> passwordResetDO = passwordResetDAO.get(resetPasswordBO.getUid());
        if(passwordResetDO.isEmpty() || passwordResetDO.get().isDead()) {
            log.error("Password reset link have already been used or is invalid");
            throw new InvalidRequestException("Password reset session was not created or expired.");
        }

        Optional<UserDO> user = userDAO.get(passwordResetDO.get().getUserId());
        if (user.isEmpty()) {
            log.error("User doesn't exist in the system");
            throw new InvalidRequestException("User does not exists in the system");
        }

        String salt = user.get().getSalt();
        String secret = passwordHasher.hash(resetPasswordBO.getPassword(), salt);

        UserDO userDO = user.get();
        userDO.setSecret(secret);
        userDAO.save(userDO);

        invalidatePasswordReset(passwordResetDO.get());
        return Response.ok("Password is successfully set to new value").build();
    }

    private void invalidatePasswordReset(PasswordResetDO passwordResetDO) {
        log.debug("Invalidating password resetting object for user {}", passwordResetDO.getUserId());
        passwordResetDO.setDead(true);
        passwordResetDAO.save(passwordResetDO);
    }

    public Response login(CredentialBO credentials) {

        log.debug("Logging into system for user {}", credentials.getUsername());
        Optional<UserDO> userDO = userDAO.get(credentials.getUsername());
        if (userDO.isEmpty()) {
            log.error("User with username {} is not present in the system", userDO.get().getEmail());
            throw new UserNotRegisteredException("User is not found in the system");
        }

        if (UserDO.Status.DEACTIVATED.equals(userDO.get().getStatus())
            || UserDO.Status.SUSPENDED.equals(userDO.get().getStatus())) {
            log.error("Account with username {} have been suspended", userDO.get().getUid());
            throw new UserAccountSuspendedOrDeactivatedException("User account have been suspended or deactivated. " +
                    "Contact to system administrator");
        }

        String digest = passwordHasher.hash(credentials.getSecret(), userDO.get().getSalt());
        if (!digest.equals(userDO.get().getSecret())) {
            log.error("Wrong username or password");
            throw new UsernamePasswordNotMatchedException("Wrong username or password");
        }

        SessionBO session = sessionManager.startSession(credentials.getUsername());
        LoginResponseBO loginResponseBO = LoginResponseBO.builder()
                .message("User logged in successfully")
                .status(1)
                .data(userTranslator.translate(userDO.get()))
                .build();
        log.info("User {} is logged in successfully", userDO.get().getUid());
        return Response.ok(loginResponseBO)
                .cookie(new NewCookie(Constants.cookieSessionKey, session.getSessionId()),
                        new NewCookie(Constants.cookieUsernameKey, session.getUsername()))
                .build();
    }

    /**
     * Register a non-scholar user for the system like, Admin, Reception etc
     *
     * @param user to be registered
     * @return
     */
    public Response registerUser(UserBO user) {
        log.info("Registering new user with username {}", user.getUid());
        createUser(user);
        passwordResetLinkGenerator.generateLink(user.getUid());
        return Response.ok("User registered successfully").build();
    }

    /**
     * Logout user with given username
     *
     * @param sessionId
     * @return
     */
    public Response logout(String sessionId) {
        log.info("User with session {} is logged out from system", sessionId);
        sessionManager.terminateSession(sessionId);
        return Response.ok("User is successfully logged out").build();
    }

    /**
     * Deactivate an account and terminate all sessions related to user.
     *
     * @param username account to be deactivated
     * @return
     */
    public Response deactivate(String username) {

        sessionManager.terminateAllSessions(username);

        Optional<UserDO> userDO = userDAO.get(username);
        if (userDO.isEmpty()) {
            log.error("User doesn't exists in the system");
            throw new UserNotRegisteredException("User doesn't exists");
        } else if (userDO.get().getStatus() == UserDO.Status.DEACTIVATED ||
                userDO.get().getStatus() == UserDO.Status.SUSPENDED) {
            log.error("User account is already suspended or deactivated");
            throw new InvalidRequestException("User account has already been suspended or deactivated state.");
        }
        UserDO user = userDO.get();
        user.setStatus(UserDO.Status.DEACTIVATED);
        userDAO.save(user);

        Optional<NotificationDO> notification = notificationDAO.get(NotificationBO.Type.USER_ACCOUNT_DEACTIVATION.name());
        if (notification.isEmpty()) {
            log.error("Notification template is not available for type {}",NotificationBO.Type.USER_ACCOUNT_DEACTIVATION);
            throw new EngineBreakageException("Notification template is not available for the given type.");
        }

        String subject = notification.get().getSubject().replace("$name", user.getName());
        String htmlBody = notification.get().getEmailTemplate().replace("$name", user.getName());
        String textBody = notification.get().getTextTemplate().replace("$name", user.getName());

        emailNotifier.send(Constants.SYSTEM_EMAIL_ID, user.getEmail(), subject, htmlBody, textBody);
        log.debug("Email have been sent to user {} for deactivation of account", username);

        log.info("User {} have been deactivated", username);
        return Response.ok("Account is successfully deactivated").build();
    }

    /**
     * Reactivate a deactivated/suspended account.
     *
     * @param username account to be activated
     *
     * @return
     */
    public Response reactivate(String username) {

        Optional<UserDO> userDO = userDAO.get(username);
        if (userDO.isEmpty()) {
            log.error("No account registered for user {}", username);
            throw new UserNotRegisteredException("User doesn't exists");
        }
        UserDO user = userDO.get();
        user.setStatus(UserDO.Status.ACTIVE);
        userDAO.save(user);

        Optional<NotificationDO> notification = notificationDAO.get(NotificationBO.Type.USER_ACCOUNT_REACTIVATION.name());
        if (notification.isEmpty()) {
            log.error("Notification template is not available for type {}",NotificationBO.Type.USER_ACCOUNT_DEACTIVATION);
            throw new EngineBreakageException("The notification template is not available into the system");
        }

        String subject = notification.get().getSubject();
        String htmlBody = notification.get().getEmailTemplate();
        String textBody = notification.get().getTextTemplate();
        subject = subject.replace("$name", user.getName());
        htmlBody = htmlBody.replace("$name", user.getName());
        textBody = textBody.replace("$name", user.getName());

        emailNotifier.send(Constants.SYSTEM_EMAIL_ID, userDO.get().getEmail(), subject, htmlBody, textBody);
        log.info("Email have been sent for user {} account reactivation", user.getUid());
        return Response.ok("Account is successfully reactivated").build();
    }
}
