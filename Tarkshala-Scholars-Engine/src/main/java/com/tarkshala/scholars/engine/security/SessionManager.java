package com.tarkshala.scholars.engine.security;

import com.tarkshala.scholars.engine.dao.EntityDAO;
import com.tarkshala.scholars.engine.dao.dynamodb.Filter;
import com.tarkshala.scholars.engine.dao.model.SessionDO;
import com.tarkshala.scholars.engine.dao.model.UserDO;
import com.tarkshala.scholars.engine.exception.UserNotRegisteredException;
import com.tarkshala.scholars.engine.models.SessionBO;
import com.tarkshala.scholars.engine.time.TimeConverter;
import com.tarkshala.scholars.engine.webservices.Constants;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class SessionManager {

    @Autowired
    private EntityDAO<UserDO> userDAO;

    @Autowired
    private EntityDAO<SessionDO> sessionDAO;

    public SessionBO startSession(String username) {

        String sessionId = RandomStringUtils.randomAlphanumeric(8);
        LocalDateTime expiryTimestamp = LocalDateTime.now().plusHours(48);

        Optional<UserDO> userDO = userDAO.get(username);
        if (userDO.isEmpty()) {
            throw new UserNotRegisteredException("User is not registered into the system");
        }

        SessionDO sessionDO = SessionDO.builder()
                .uid(sessionId)
                .user(userDO.get())
                .expiryTimestamp(TimeConverter.convertToLong(expiryTimestamp))
                .build();

        sessionDAO.save(sessionDO);
        return SessionBO.builder().sessionId(sessionId).username(userDO.get().getUid()).build();
    }

    public void terminateSession(String sessionId) {
        Optional<SessionDO> sessionDO = sessionDAO.get(sessionId);
        if (sessionDO.isEmpty()) {
            return;
        }
        sessionDAO.delete(sessionDO.get());
    }

    @SneakyThrows
    public void terminateAllSessions(String username) {

        Filter filter = Filter.builder()
                .attributeType(Filter.AttributeType.STRING)
                .attributeName("user.uid")
                .operation(Filter.Operation.EQ)
                .attributeValue(username)
                .build();
        List<SessionDO> availableSessions = sessionDAO.get(List.of(filter));
        availableSessions.forEach(sessionDO -> {
            sessionDAO.delete(sessionDO);
        });
    }

    public void extendSession(String sessionId) {
        Optional<SessionDO> sessionDO = sessionDAO.get(sessionId);
        if (sessionDO.isEmpty() || sessionDO.get().getExpiryTimestamp() < System.currentTimeMillis()) {
            log.debug("Session has already expired, it can't be extended");
            return;
        }
        extendSession(sessionDO.get());
    }

    private void extendSession(SessionDO session) {
        LocalDateTime expiryTimestamp = LocalDateTime.now().plusHours(Constants.SESSION_TIME);
        session.setExpiryTimestamp(TimeConverter.convertToLong(expiryTimestamp));
        sessionDAO.save(session);
    }
}
