package com.tarkshala.scholars.engine.webservices;

public class Constants {

    public final static String cookieSessionKey = "SESSION_ID";
    public final static String cookieUsernameKey = "USERNAME";
    public final static String UID = "uid";
    public final static String SYSTEM_EMAIL_ID = "tarkshala@gmail.com";

    public final static String DIGICALM_SMS_API_URL = "http://sms.digicalmmedia.com/api/SendMesssgeAPI";
    public final static String DIGICALM_USERNAME = "Kuldeep01";
    public final static String DIGICALM_PASSWORD = "Kuldeep01";
    public final static String DIGICALM_SENDER_ID = "TRKSLA";
    public final static String DIGICALM_DATA_CODING = "0";
    public final static String DIGICALM_GWID = "1";

    public final static long SESSION_TIME = 4 * 24; // time in hours
}
