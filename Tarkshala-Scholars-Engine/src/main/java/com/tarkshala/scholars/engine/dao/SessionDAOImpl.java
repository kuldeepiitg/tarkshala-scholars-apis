package com.tarkshala.scholars.engine.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.tarkshala.scholars.engine.dao.dynamodb.Filter;
import com.tarkshala.scholars.engine.dao.dynamodb.FilterExpression;
import com.tarkshala.scholars.engine.dao.model.SessionDO;
import com.tarkshala.scholars.engine.webservices.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SessionDAOImpl extends EntityDAO<SessionDO> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public Optional<SessionDO> get(String id) {
        return Optional.ofNullable(dynamoDBMapper.load(SessionDO.class, id));
    }

    @Override
    public List<SessionDO> get(List<Filter> filters, String previousPageLastKey, int count) {

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withLimit(count);
        if (filters != null && !filters.isEmpty()) {
            FilterExpression filterExpression = new FilterExpression(filters);
            scanExpression.setFilterExpression(filterExpression.getFilterExpression());
            scanExpression.setExpressionAttributeNames(filterExpression.getAttributeNames());
            scanExpression.setExpressionAttributeValues(filterExpression.getAttributeValues());
        }

        if (previousPageLastKey != null) {
            Map<String, AttributeValue> exclusiveStartKey = new HashMap<>();
            exclusiveStartKey.put(Constants.UID, new AttributeValue().withS(previousPageLastKey));
            scanExpression.setExclusiveStartKey(exclusiveStartKey);
        }

        List<SessionDO> sessions = new ArrayList<>();
        Iterator<SessionDO> iterator = dynamoDBMapper.scan(SessionDO.class, scanExpression).iterator();
        while (iterator.hasNext() && count > 0) {
            sessions.add(iterator.next());
            count--;
        }
        return sessions;
    }

    @Override
    public List<SessionDO> get(List<Filter> filters) {
        return get(filters, null, Integer.MAX_VALUE);
    }
}
