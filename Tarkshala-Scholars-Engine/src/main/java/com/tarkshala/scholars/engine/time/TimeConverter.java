package com.tarkshala.scholars.engine.time;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.TimeZone;

public class TimeConverter {

    private static int gmtOffet = 19800;

    public static long convertToLong(LocalDateTime localDateTime) {
        return localDateTime.toInstant(ZoneOffset.ofTotalSeconds(gmtOffet)).toEpochMilli();
    }

    public LocalDateTime convertToLocalDateTime(long localDateTime) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(localDateTime),
                TimeZone.getTimeZone("IST").toZoneId());
    }
}
