package com.tarkshala.scholars.engine.webservices.converters;

import com.tarkshala.scholars.engine.dao.model.TransactionDO;
import com.tarkshala.scholars.engine.models.TransactionBO;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TransactionTranslatorImpl implements Translator<TransactionDO, TransactionBO> {

    @Override
    public TransactionBO translate(TransactionDO entityDO) {
        return TransactionBO.builder()
                .uid(entityDO.getUid())
                .scholarUid(entityDO.getScholarUid())
                .amount(entityDO.getAmount())
                .transactionType(entityDO.getTransactionType().name())
                .modeOfPayment(entityDO.getModeOfPayment())
                .transactionId(entityDO.getTransactionId())
                .paidBy(entityDO.getPaidBy())
                .paidTo(entityDO.getPaidTo())
                .dueDate(entityDO.getDueDate().getTime())
                .build();
    }

    @Override
    public TransactionDO translate(TransactionBO entityBO) {

        return TransactionDO.builder()
                .uid(entityBO.getUid())
                .scholarUid(entityBO.getScholarUid())
                .amount(entityBO.getAmount())
                .transactionType(TransactionDO.TransactionType.valueOf(entityBO.getTransactionType()))
                .modeOfPayment(entityBO.getModeOfPayment())
                .transactionId(entityBO.getTransactionId())
                .paidBy(entityBO.getPaidBy())
                .paidTo(entityBO.getPaidTo())
                .dueDate(new Date(entityBO.getDueDate()))
                .build();
    }
}
