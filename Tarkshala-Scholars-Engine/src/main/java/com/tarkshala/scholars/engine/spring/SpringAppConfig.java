package com.tarkshala.scholars.engine.spring;

import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:spring-config.xml")
public class SpringAppConfig {

    @Bean
    public AmazonDynamoDBClientBuilder getAmazonDynamoDBClientBuilder() {
        EnvironmentVariableCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider();
        AmazonDynamoDBClientBuilder amazonDynamoDBClientBuilder =
                AmazonDynamoDBClientBuilder.standard().withRegion(Regions.AP_SOUTH_1);
        amazonDynamoDBClientBuilder.setCredentials(credentialsProvider);
        return amazonDynamoDBClientBuilder;
    }

    @Bean
    public DynamoDBMapper getDynamoDBMapper(final AmazonDynamoDBClientBuilder builder) {
        return new DynamoDBMapper(builder.build());
    }

    @Bean
    public DynamoDB getDynamoDB(final AmazonDynamoDBClientBuilder builder) {
        return new DynamoDB(builder.build());
    }

    @Bean
    public AmazonSimpleEmailService getAmazonSimpleEmailServiceClientBuilder() {
        return AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.AP_SOUTH_1)
                .withCredentials(new EnvironmentVariableCredentialsProvider()).build();
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }
}
