package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.models.NotificationBO;
import com.tarkshala.scholars.engine.security.Role;
import com.tarkshala.scholars.engine.spring.SpringApplication;
import com.tarkshala.scholars.engine.webservices.filters.Authorization;
import com.tarkshala.scholars.engine.webservices.filters.PubliclyAllowed;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/configuration")
@Authorization
public class ConfigurationService extends SpringApplication {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Path("/greet")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @PubliclyAllowed
    public Response greet(){
        logger.info("Welcome to Tarkshala Scholar Configuration APIs");
        return getBean(AuthenticationServiceHandler.class).greet();
    }


    @Path("/notification/configure")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response configureNotification(NotificationBO notificationBO) {
        return getBean(ConfigurationServiceHandler.class).configureNotification(notificationBO);
    }

    @Path("/notification")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response getNotification(String type) {
        return getBean(ConfigurationServiceHandler.class).getNotification(type);
    }

    @Path("/notification/types")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response getNotificationTypes() {
        return getBean(ConfigurationServiceHandler.class).getNotificationTypes();
    }

}
