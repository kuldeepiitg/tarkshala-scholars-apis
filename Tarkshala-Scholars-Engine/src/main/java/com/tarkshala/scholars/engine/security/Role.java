package com.tarkshala.scholars.engine.security;

/**
 * Role is basically acting as good as an enum,
 *
 * we can't keep it enum as these will be used into @annotations
 * like {@code @RolesAllowed(Role.ADMINISTRATOR)}, annotations don't permit enums as values.
 */
public class Role {
    public static final String STUDENT = "STUDENT";
    public static final String ADMINISTRATOR = "ADMINISTRATOR";
    public static final String SYSTEM_ADMINISTRATOR = "SYSTEM_ADMINISTRATOR";
    public static final String SYSTEM = "SYSTEM";

    public static String[] getRoles() {
        return new String[] {STUDENT, ADMINISTRATOR, SYSTEM_ADMINISTRATOR, SYSTEM};
    }
}
