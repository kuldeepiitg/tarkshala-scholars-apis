package com.tarkshala.scholars.engine.exception;

public class UsernamePasswordNotMatchedException extends RuntimeException {

    public UsernamePasswordNotMatchedException(String message) {
        super(message);
    }
}
