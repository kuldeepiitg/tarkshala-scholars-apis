package com.tarkshala.scholars.engine.webservices.exception.mapper;

import com.tarkshala.scholars.engine.exception.UserAlreadyRegisteredException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UserAlreadyRegisteredExceptionHandler implements ExceptionMapper<UserAlreadyRegisteredException> {
    @Override
    public Response toResponse(UserAlreadyRegisteredException exception) {
        return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
    }
}
