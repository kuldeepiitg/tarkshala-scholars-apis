package com.tarkshala.scholars.engine.exception;

public class InvalidUsernameException  extends RuntimeException{

    public InvalidUsernameException(String message) {
        super(message);
    }
}
