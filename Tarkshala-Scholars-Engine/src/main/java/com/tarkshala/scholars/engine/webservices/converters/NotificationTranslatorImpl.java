package com.tarkshala.scholars.engine.webservices.converters;

import com.tarkshala.scholars.engine.dao.model.NotificationDO;
import com.tarkshala.scholars.engine.models.NotificationBO;
import org.springframework.stereotype.Component;

@Component
public class NotificationTranslatorImpl implements Translator<NotificationDO, NotificationBO> {

    @Override
    public NotificationBO translate(NotificationDO entityDO) {
        return NotificationBO.builder()
                .subject(entityDO.getSubject())
                .emailTemplate(entityDO.getEmailTemplate())
                .textTemplate(entityDO.getTextTemplate())
                .type(NotificationBO.Type.valueOf(entityDO.getType()))
                .build();
    }

    @Override
    public NotificationDO translate(NotificationBO entityBO) {
        return NotificationDO.builder()
                .subject(entityBO.getSubject())
                .emailTemplate(entityBO.getEmailTemplate())
                .textTemplate(entityBO.getTextTemplate())
                .type(entityBO.getType().name())
                .build();
    }
}
