package com.tarkshala.scholars.engine.webservices.converters;

import com.tarkshala.scholars.engine.dao.model.UserDO;
import com.tarkshala.scholars.engine.models.UserBO;
import org.springframework.stereotype.Component;

@Component
public class UserTranslatorImpl implements Translator<UserDO, UserBO> {

    @Override
    public UserBO translate(UserDO entityDO) {
        return UserBO.builder()
                .uid(entityDO.getUid())
                .name(entityDO.getName())
                .roles(entityDO.getRoles())
                .build();
    }

    @Override
    public UserDO translate(UserBO entityBO) {
        // FIXME: 21/02/20 This method is not supposed to be implemented.
        return null;
    }
}
