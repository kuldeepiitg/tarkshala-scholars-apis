package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.dao.EntityDAO;
import com.tarkshala.scholars.engine.dao.TransactionDAOImpl;
import com.tarkshala.scholars.engine.dao.model.NotificationDO;
import com.tarkshala.scholars.engine.dao.model.StudentDO;
import com.tarkshala.scholars.engine.dao.model.TransactionDO;
import com.tarkshala.scholars.engine.exception.EngineBreakageException;
import com.tarkshala.scholars.engine.exception.EntityMissingException;
import com.tarkshala.scholars.engine.exception.InvalidRequestException;
import com.tarkshala.scholars.engine.exception.UserNotRegisteredException;
import com.tarkshala.scholars.engine.models.InvoiceBO;
import com.tarkshala.scholars.engine.models.NotificationBO;
import com.tarkshala.scholars.engine.models.StudentBO;
import com.tarkshala.scholars.engine.models.TransactionBO;
import com.tarkshala.scholars.engine.notification.EmailNotifier;
import com.tarkshala.scholars.engine.notification.SMSNotifier;
import com.tarkshala.scholars.engine.webservices.converters.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class AccountServiceHandler {

    @Autowired
    private EntityDAO<StudentDO> studentDAO;

    @Autowired
    private EntityDAO<TransactionDO> transactionDAO;

    @Autowired
    private EntityDAO<NotificationDO> notificationDAO;

    @Autowired
    private Translator<TransactionDO, TransactionBO> transactionTranslator;

    @Autowired
    private Translator<StudentDO, StudentBO> studentTranslator;

    @Autowired
    private EmailNotifier emailNotifier;

    @Autowired
    private SMSNotifier smsNotifier;

    public Response getProfile(String username) {

        Optional<StudentDO> studentDO = studentDAO.get(username);
        if (studentDO.isEmpty()) {
            throw new UserNotRegisteredException("User with given name doesn't exists.");
        }
        StudentBO studentBO = studentTranslator.translate(studentDO.get());
        return Response.ok(studentBO).build();
    }

    public Response getInvoice(String scholarId) {
        Optional<List<TransactionDO>> transactions = ((TransactionDAOImpl) transactionDAO).getAllForStudent(scholarId);
        if (transactions.isPresent()) {
            List<TransactionDO> transactionList = transactions.get();
            List<TransactionBO> transactionBOS = transactionList.stream()
                    .map(transaction -> transactionTranslator.translate(transaction))
                    .collect(Collectors.toList());
            InvoiceBO invoice = InvoiceBO.builder().transactions(transactionBOS).build();
            return Response.ok(invoice).build();
        }
        return Response.ok(new InvoiceBO()).build();
    }

    public Response createTransaction(TransactionBO transaction) {

        TransactionDO transactionDO = transactionTranslator.translate(transaction);

        Optional<StudentDO> student = studentDAO.get(transaction.getScholarUid());
        if (student.isEmpty()) {
            throw new InvalidRequestException("Student is not available in the system record.");
        }

        transactionDAO.save(transactionDO);

        Optional<NotificationDO> notification;
        if (TransactionDO.TransactionType.CREDIT == transactionDO.getTransactionType()) {
            notification = notificationDAO.get(NotificationBO.Type.FINANCE_ACCOUNT_CREDIT.name());
        } else if (TransactionDO.TransactionType.DEBIT == transactionDO.getTransactionType()) {
            notification = notificationDAO.get(NotificationBO.Type.FINANCE_ACCOUNT_DEBIT.name());
        } else if (TransactionDO.TransactionType.FINE == transactionDO.getTransactionType()) {
            notification = notificationDAO.get(NotificationBO.Type.FINANCE_ACCOUNT_FINE.name());
        } else {
            throw new EngineBreakageException("Notification template is not configured for this.");
        }

        if (notification.isEmpty()) {
            throw new EngineBreakageException("Notification template is not configured for this.");
        }

        String subject = notification.get().getSubject();
        String emailBody = notification.get().getEmailTemplate();
        String textBody = notification.get().getTextTemplate();

        subject = subject.replace("$amount", Integer.toString(transaction.getAmount()));

        emailBody = emailBody.replace("$name", student.get().getName());
        emailBody = emailBody.replace("$amount", Integer.toString(transaction.getAmount()));
        emailBody = emailBody.replace("$type", transaction.getTransactionType());

        textBody = textBody.replace("$name", student.get().getName());
        textBody = textBody.replace("$amount", Integer.toString(transaction.getAmount()));
        textBody = textBody.replace("$type", transaction.getTransactionType());

        emailNotifier.send(Constants.SYSTEM_EMAIL_ID, transactionDO.getScholarUid(), subject, textBody, emailBody);
        final String finalTextBody = textBody;
        final String finalSubject = subject;
        smsNotifier.send(student.get().getPhones().values(), finalSubject, finalTextBody);
        return Response.ok("Transaction is saved successfully").build();
    }

    public Response getTransaction(String uid) {
        Optional<TransactionDO> transaction = transactionDAO.get(uid);
        if (transaction.isEmpty()) {
            throw new EntityMissingException("Transaction with given uid is not present");
        }
        TransactionBO transactionBO = transactionTranslator.translate(transaction.get());
        return Response.ok(transactionBO).build();
    }
}
