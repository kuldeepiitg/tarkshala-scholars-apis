package com.tarkshala.scholars.engine.dao.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@DynamoDBTable(tableName = "students")
@DynamoDBDocument
public class StudentDO extends EntityDO {

    @DynamoDBAttribute
    private String name;

    @DynamoDBAttribute
    private String parentName;

    @DynamoDBAttribute
    private String email;

    @DynamoDBAttribute
    private Map<String, String> phones;

    @DynamoDBAttribute
    private String standard;

    @DynamoDBAttribute
    private String school;

    @DynamoDBAttribute
    private String address;

    @DynamoDBAttribute
    private Date joiningDate;

    @DynamoDBAttribute
    private Set<String> subjects;

    @DynamoDBAttribute
    private String remark;

    @DynamoDBAttribute
    private String foundUsOn;
}
