package com.tarkshala.scholars.engine.webservices.converters;

import com.tarkshala.scholars.engine.dao.model.StudentDO;
import com.tarkshala.scholars.engine.models.StudentBO;
import org.springframework.stereotype.Component;

@Component
public class StudentTranslatorImpl implements Translator<StudentDO, StudentBO> {
    @Override
    public StudentBO translate(StudentDO entityDO) {
        return StudentBO.builder()
                .uid(entityDO.getUid())
                .name(entityDO.getName())
                .parentName(entityDO.getParentName())
                .email(entityDO.getEmail())
                .phones(entityDO.getPhones())
                .standard(entityDO.getStandard())
                .school(entityDO.getSchool())
                .address(entityDO.getAddress())
                .joiningDate(entityDO.getJoiningDate())
                .subjects(entityDO.getSubjects())
                .remark(entityDO.getRemark())
                .foundUsOn(entityDO.getFoundUsOn())
                .build();
    }

    @Override
    public StudentDO translate(StudentBO entityBO) {
        return StudentDO.builder()
                .uid(entityBO.getUid())
                .name(entityBO.getName())
                .parentName(entityBO.getParentName())
                .email(entityBO.getEmail())
                .phones(entityBO.getPhones())
                .standard(entityBO.getStandard())
                .school(entityBO.getSchool())
                .address(entityBO.getAddress())
                .joiningDate(entityBO.getJoiningDate())
                .subjects(entityBO.getSubjects())
                .remark(entityBO.getRemark())
                .foundUsOn(entityBO.getFoundUsOn())
                .build();
    }
}
