package com.tarkshala.scholars.engine.webservices.exception.mapper;

import com.tarkshala.scholars.engine.exception.InvalidUsernameException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvalidUsernameExceptionHandler implements ExceptionMapper<InvalidUsernameException> {
    @Override
    public Response toResponse(InvalidUsernameException exception) {
        return Response.status(Response.Status.FORBIDDEN).entity(exception.getMessage()).build();
    }
}
