package com.tarkshala.scholars.engine.dao.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@DynamoDBTable(tableName = "student-ledger")
@DynamoDBDocument
public class TransactionDO extends EntityDO {

    @DynamoDBAttribute
    private String scholarUid;

    @DynamoDBAttribute
    private int amount;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private TransactionType transactionType;

    // Only for credit transaction type
    @DynamoDBAttribute
    private String paidBy;

    // Only for credit transaction type
    @DynamoDBAttribute
    private String paidTo;

    // Only for credit transaction type
    @DynamoDBAttribute
    private String modeOfPayment;

    // Only for credit transaction type
    @DynamoDBAttribute
    private String transactionId;

    @DynamoDBAttribute
    private String remark;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEpochDate
    private Date dueDate;

    @DynamoDBDocument
    public enum TransactionType {
        CREDIT, DEBIT, SERVICE, FINE
    }
}
