package com.tarkshala.scholars.engine.dao;


import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.tarkshala.scholars.engine.dao.dynamodb.Filter;
import com.tarkshala.scholars.engine.dao.dynamodb.FilterExpression;
import com.tarkshala.scholars.engine.dao.model.EntityDO;
import com.tarkshala.scholars.engine.webservices.Constants;
import org.apache.http.MethodNotSupportedException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class EntityDAO<T extends EntityDO> implements DAO<T> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public void save(T entity) {
        dynamoDBMapper.save(entity);
    }

    @Override
    public void delete(T entity) {
        dynamoDBMapper.delete(entity);
    }

    @Override
    public List<T> get(List<Filter> filters, String previousPageLastKey, int count) throws MethodNotSupportedException {
        throw new MethodNotSupportedException("Scanning using filters is not provided by default.");
    }

    @Override
    public List<T> get(List<Filter> filters) throws MethodNotSupportedException {
        throw new MethodNotSupportedException("Scanning using filters is not provided by default.");
    }

    protected DynamoDBScanExpression getDynamoDBScanExpression(List<Filter> filters, String previousPageLastKey, int count) {
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withLimit(count);
        if (filters != null && !filters.isEmpty()) {
            FilterExpression filterExpression = new FilterExpression(filters);
            scanExpression.setFilterExpression(filterExpression.getFilterExpression());
            scanExpression.setExpressionAttributeNames(filterExpression.getAttributeNames());
            scanExpression.setExpressionAttributeValues(filterExpression.getAttributeValues());
        }

        if (previousPageLastKey != null) {
            Map<String, AttributeValue> exclusiveStartKey = new HashMap<>();
            exclusiveStartKey.put(Constants.UID, new AttributeValue().withS(previousPageLastKey));
            scanExpression.setExclusiveStartKey(exclusiveStartKey);
        }
        return scanExpression;
    }
}
