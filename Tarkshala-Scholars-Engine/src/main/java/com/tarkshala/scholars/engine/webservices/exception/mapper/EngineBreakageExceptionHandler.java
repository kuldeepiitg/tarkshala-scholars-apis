package com.tarkshala.scholars.engine.webservices.exception.mapper;

import com.tarkshala.scholars.engine.exception.EngineBreakageException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EngineBreakageExceptionHandler implements ExceptionMapper<EngineBreakageException> {

    @Override
    public Response toResponse(EngineBreakageException exception) {

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("Oops, looks like we need to train our Technology team better. We will correct it soon."
                        + exception.getMessage()).build();
    }
}
