package com.tarkshala.scholars.engine.exception;

public class UserAccountSuspendedOrDeactivatedException extends RuntimeException {

    public UserAccountSuspendedOrDeactivatedException(String message) {
        super(message);
    }
}
