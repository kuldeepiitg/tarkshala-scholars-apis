package com.tarkshala.scholars.engine.webservices.exception.mapper;

import com.tarkshala.scholars.engine.exception.UserNotRegisteredException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UserNotRegisteredExceptionHandler implements ExceptionMapper<UserNotRegisteredException> {

    @Override
    public Response toResponse(UserNotRegisteredException exception) {
        return Response.status(Response.Status.UNAUTHORIZED).entity(exception.getMessage()).build();
    }
}
