package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.dao.EntityDAO;
import com.tarkshala.scholars.engine.dao.dynamodb.Filter;
import com.tarkshala.scholars.engine.dao.model.StudentDO;
import com.tarkshala.scholars.engine.models.StudentBO;
import com.tarkshala.scholars.engine.webservices.converters.Translator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class DirectoryServiceHandler {

    @Autowired
    private EntityDAO<StudentDO> studentDAO;

    @Autowired
    private Translator<StudentDO, StudentBO> studentTranslator;

    @SneakyThrows
    public Response list(int count, String lastEvaluatedKey, List<Filter> filters) {

        List<StudentDO> students = studentDAO.get(filters, lastEvaluatedKey, count);
        List<StudentBO> list = students
                .stream()
                .map(studentDO -> studentTranslator.translate(studentDO))
                .collect(Collectors.toList());
        return Response.ok(list).build();
    }
}
