package com.tarkshala.scholars.engine.dao.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@DynamoDBTable(tableName = "users")
@DynamoDBDocument
public class UserDO extends EntityDO {

    private String name;

    private String email;

    private List<String> roles;

    private String secret;

    private String salt;

    @DynamoDBTypeConvertedEnum
    private Status status;

    public enum Status {
        // By default the new user will be stranded in DEACTIVATED status.
        DEACTIVATED,

        // The user will be activated as soon as he sets his login and password
        ACTIVE,

        // The user has been suspended by administrator
        SUSPENDED,
    }
}
