package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.dao.EntityDAO;
import com.tarkshala.scholars.engine.dao.model.NotificationDO;
import com.tarkshala.scholars.engine.exception.EntityMissingException;
import com.tarkshala.scholars.engine.models.NotificationBO;
import com.tarkshala.scholars.engine.webservices.converters.Translator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.Optional;

@Slf4j
@Component
public class ConfigurationServiceHandler {

    @Autowired
    private Translator<NotificationDO, NotificationBO> notificationTranslator;

    @Autowired
    private EntityDAO<NotificationDO> notificationDAO;

    public Response configureNotification(NotificationBO notificationBO) {
        NotificationDO notification = notificationTranslator.translate(notificationBO);
        notificationDAO.save(notification);
        log.info("Notification template of type {} have been configured", notificationBO.getType());
        return Response.ok("Notification have been configured well").build();
    }

    public Response getNotification(String type) {
        Optional<NotificationDO> notification = notificationDAO.get(type);
        if (notification.isEmpty()) {
            log.info("Notification template of type {} have been configured", type);
            throw new EntityMissingException("Notification of given type is not present");
        }
        return Response.ok(notification.get()).build();
    }

    public Response getNotificationTypes() {
        log.info("Getting list of all present notification types");
        return Response.ok(NotificationBO.Type.values()).build();
    }
}
