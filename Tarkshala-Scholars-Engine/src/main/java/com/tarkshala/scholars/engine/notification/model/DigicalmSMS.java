package com.tarkshala.scholars.engine.notification.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Model for Digicalm SMS
 */
@Getter
@Setter
@AllArgsConstructor
@Builder
public class DigicalmSMS {

    /**
     * Digicalm username
     */
    @JsonProperty
    private String username;

    /**
     * Digicalm password
     */
    @JsonProperty
    private String password;

    /**
     * Digicalm sender-id, in case of tarkshala it is TRKSLA
     */
    @JsonProperty("SenderId")
    private String senderId;

    @JsonProperty("DataCoding")
    private String dataCoding;

    /**
     * 0 for promotional SMS
     * 1 for transactional SMS
     */
    @JsonProperty("Gwid")
    private String gwid;

    @JsonProperty("MobileMessage")
    private ArrayList<Message> messages;

    @Getter
    @Setter
    @AllArgsConstructor
    @Builder
    public static class Message {

        @JsonProperty("MobileNumber")
        public String mobileNumber;

        @JsonProperty
        public String message;
    }
}
