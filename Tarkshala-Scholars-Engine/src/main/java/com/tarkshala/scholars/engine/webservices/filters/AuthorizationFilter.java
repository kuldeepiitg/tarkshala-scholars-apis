package com.tarkshala.scholars.engine.webservices.filters;

import com.tarkshala.scholars.engine.dao.EntityDAO;
import com.tarkshala.scholars.engine.dao.SessionDAOImpl;
import com.tarkshala.scholars.engine.dao.model.SessionDO;
import com.tarkshala.scholars.engine.security.SessionManager;
import com.tarkshala.scholars.engine.spring.SpringApplication;
import com.tarkshala.scholars.engine.webservices.Constants;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Provider
@Authorization
public class AuthorizationFilter extends SpringApplication implements ContainerRequestFilter, ContainerResponseFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) {

        // check for @PermitAll
        if (isResourcePubliclyAllowed()) {
            log.info("Accessed resource is publicly accessible");
            return;
        }

        if (!requestContext.getHeaders().containsKey(Constants.cookieSessionKey)
            || !requestContext.getHeaders().containsKey(Constants.cookieUsernameKey)) {
            log.error("Resource can not be accessed with out login");
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                    .entity("You can't access the resource without login").build());
            return;
        }
        String requestSessionId = requestContext.getHeaders().getFirst("SESSION_ID");
        String username = requestContext.getHeaders().getFirst("USERNAME");
        EntityDAO<SessionDO> sessionDAO = getBean(SessionDAOImpl.class);
        Optional<SessionDO> session = sessionDAO.get(requestSessionId);
        if (session.isEmpty() || session.get().getExpiryTimestamp() < System.currentTimeMillis()) {
            log.error("Please login again, it seems the session you are using have already been expired");
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                    .entity("The login session has expired,  please login again to access the resource").build());
            return;
        }

        if (!username.equals(session.get().getUser().getUid())) {
            log.error("Session id doesn't match to the user, suspicious activity");
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                    .entity("Activity is suspicious, action will be reported to system admin").build());
            return;
        }

        List<String> roles = session.get().getUser().getRoles();
        if (!isResourcePermitted(roles)) {
            log.error("User doesn't have correct rights over resource");
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                    .entity("User is authenticated successfully, but doesn't have legitimate rights over the resource")
                    .build());
            return;
        }

        extendSession(requestSessionId);
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {

    }

    /**
     * Check if the role is allowed to access given resource.
     *
     * @param roles of the user trying to access the resource
     * @return false if role is not permitted to execute, otherwise true
     */
    private boolean isResourcePermitted(List<String> roles) {

        Method method = resourceInfo.getResourceMethod();
        if (method.isAnnotationPresent(DenyAll.class)) {
            return false;
        }

        if (method.isAnnotationPresent(RolesAllowed.class)) {
            HashSet<String> rolesSetAvailableOnAPI = new HashSet<>(Arrays.asList(method.getAnnotation(RolesAllowed.class).value()));

            // retain only the roles that are present in both sets
            // basically it is equivalent to set operation `intersection`
            rolesSetAvailableOnAPI.retainAll(roles);
            if (rolesSetAvailableOnAPI.isEmpty()) {
                return false;
            }
        }

        // @PermitAll will be allowed by default
        return true;
    }

    /**
     * Checks for @PermitAll annotation,
     *
     * @return true if method have @PermitAll annotation, false otherwise
     */
    private boolean isResourcePubliclyAllowed() {

        return resourceInfo.getResourceMethod().isAnnotationPresent(PubliclyAllowed.class);
    }

    private void extendSession(String sessionId) {
        // TODO: 11/04/20 Check if this causes memory leaks
        // TODO: 12/04/20 Check if this is really an async task, breakpoint inside the function
        // halts the execution
        CompletableFuture.runAsync(() -> {
            // method call or code to be asynch.
            getBean(SessionManager.class).extendSession(sessionId);
        });
    }
}
