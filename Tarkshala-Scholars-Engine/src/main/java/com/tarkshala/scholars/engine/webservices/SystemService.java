package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.security.Role;
import com.tarkshala.scholars.engine.spring.SpringApplication;
import com.tarkshala.scholars.engine.webservices.filters.Authorization;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;


@Path("/system")
@Authorization
public class SystemService extends SpringApplication {

    @Path("/dodgers/notify")
    @POST
    @RolesAllowed(Role.SYSTEM)
    public Response notifyDodgers() {
        return getBean(SystemServiceHandler.class).notifyDodgers();
    }
}
