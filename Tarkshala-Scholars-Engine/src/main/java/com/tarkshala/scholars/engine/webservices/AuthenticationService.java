package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.models.CredentialBO;
import com.tarkshala.scholars.engine.models.ResetPasswordBO;
import com.tarkshala.scholars.engine.models.StudentBO;
import com.tarkshala.scholars.engine.models.UserBO;
import com.tarkshala.scholars.engine.security.Role;
import com.tarkshala.scholars.engine.spring.SpringApplication;
import com.tarkshala.scholars.engine.webservices.filters.Authorization;
import com.tarkshala.scholars.engine.webservices.filters.Logged;
import com.tarkshala.scholars.engine.webservices.filters.PubliclyAllowed;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/authentication")
@Authorization
@Logged
public class AuthenticationService extends SpringApplication {

    @Path("/greet")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @PubliclyAllowed
    public Response greet(){
        return getBean(AuthenticationServiceHandler.class).greet();
    }

    @Path("/register/scholar")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Authorization
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response registerStudent(StudentBO student) {
        return getBean(AuthenticationServiceHandler.class).registerScholar(student);
    }

    @Path("/roles")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response getAvailableUserRoles() {
        return getBean(AuthenticationServiceHandler.class).getAvailableUserRoles();
    }

    @Path("/register/user")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Authorization
    @RolesAllowed(Role.ADMINISTRATOR)
    @PubliclyAllowed
    public Response registerUser(UserBO userBO) {
        return getBean(AuthenticationServiceHandler.class).registerUser(userBO);
    }

    @Path("/password/forgot")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    @PubliclyAllowed
    public Response forgotPassword(String uid) {
        return getBean(AuthenticationServiceHandler.class).forgotPassword(uid);
    }

    @Path("/password/reset")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @PubliclyAllowed
    public Response resetPassword(ResetPasswordBO resetPasswordBO) {
        return getBean(AuthenticationServiceHandler.class).resetPassword(resetPasswordBO);
    }

    @Path("/login")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Logged
    @PubliclyAllowed
    public Response login(CredentialBO credentials) {
        return getBean(AuthenticationServiceHandler.class).login(credentials);
    }

    @Path("/logout")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Authorization
    @PermitAll
    public Response logout(@Context ContainerRequestContext containerRequestContext) {
        String sessionId = containerRequestContext.getHeaders().getFirst(Constants.cookieSessionKey);
        return getBean(AuthenticationServiceHandler.class).logout(sessionId);
    }

    @Path("/deactivate")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Authorization
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response deactivate(String username){
        return getBean(AuthenticationServiceHandler.class).deactivate(username);
    }

    @Path("/reactivate")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Authorization
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response reactivate(String username) {
        return getBean(AuthenticationServiceHandler.class).reactivate(username);
    }
}
