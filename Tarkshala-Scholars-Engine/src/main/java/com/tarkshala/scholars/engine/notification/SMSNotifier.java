package com.tarkshala.scholars.engine.notification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkshala.scholars.engine.exception.EngineBreakageException;
import com.tarkshala.scholars.engine.notification.model.DigicalmSMS;
import com.tarkshala.scholars.engine.webservices.Constants;
import lombok.SneakyThrows;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class SMSNotifier {

    @Autowired
    private ObjectMapper objectMapper;

    public void send(Collection<String> to, String subject, String textBody) {

        ArrayList<DigicalmSMS.Message> messages = new ArrayList<>();
        for (String phoneNumber: to) {
            messages.add(DigicalmSMS.Message.builder()
                    .mobileNumber(phoneNumber.replace("+91", ""))
                    .message(textBody)
                    .build());
        }

        DigicalmSMS digicalmSMS = DigicalmSMS.builder()
                .username(Constants.DIGICALM_USERNAME)
                .password(Constants.DIGICALM_PASSWORD)
                .senderId(Constants.DIGICALM_SENDER_ID)
                .dataCoding(Constants.DIGICALM_DATA_CODING)
                .gwid(Constants.DIGICALM_GWID)
                .messages(messages)
                .build();

        HttpPost postRequest = new HttpPost(URI.create(Constants.DIGICALM_SMS_API_URL));
        postRequest.addHeader("Content-Type", "application/json");
        StringEntity entity;
        try {
            entity = new StringEntity(objectMapper.writeValueAsString(digicalmSMS));
        } catch (Exception e) {
            throw new EngineBreakageException(e.getMessage());
        }
        postRequest.setEntity(entity);

        HttpClient client = HttpClientBuilder.create().build();
        try {
            client.execute(postRequest);
        } catch (IOException e) {
            throw new EngineBreakageException(e.getMessage());
        }
    }
}
