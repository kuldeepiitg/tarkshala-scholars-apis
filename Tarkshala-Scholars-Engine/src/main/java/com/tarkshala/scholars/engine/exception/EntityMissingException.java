package com.tarkshala.scholars.engine.exception;

public class EntityMissingException extends RuntimeException {

    public EntityMissingException(String message) {
        super(message);
    }
}
