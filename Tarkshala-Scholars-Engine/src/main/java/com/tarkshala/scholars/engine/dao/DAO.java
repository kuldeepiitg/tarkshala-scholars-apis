package com.tarkshala.scholars.engine.dao;

import com.tarkshala.scholars.engine.dao.dynamodb.Filter;
import com.tarkshala.scholars.engine.dao.model.EntityDO;
import org.apache.http.MethodNotSupportedException;

import java.util.List;
import java.util.Optional;

public interface DAO<T extends EntityDO> {

    Optional<T> get(String id);

    /**
     * Working for both create and update.
     *
     * @param entity to be saved
     */
    void save(T entity);

    void delete(T entity);

    List<T> get(List<Filter> filters, String previousPageLastKey, int count) throws MethodNotSupportedException;

    List<T> get(List<Filter> filters) throws MethodNotSupportedException;
}
