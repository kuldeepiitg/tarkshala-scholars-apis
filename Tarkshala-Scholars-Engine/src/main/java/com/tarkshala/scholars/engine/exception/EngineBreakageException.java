package com.tarkshala.scholars.engine.exception;

/**
 * The exception tells that system failure happened due to some
 * code bug.
 */
public class EngineBreakageException extends RuntimeException {

    public EngineBreakageException(String message) {
        super(message);
    }
}
