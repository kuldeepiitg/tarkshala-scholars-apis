package com.tarkshala.scholars.engine.webservices.filters;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A resource annotated with {@code PubliclyAllowed} will have no security check. APIs
 * like login or greet should be annotated with this.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface PubliclyAllowed { }
