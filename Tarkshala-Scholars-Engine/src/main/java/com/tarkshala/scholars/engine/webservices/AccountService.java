package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.models.TransactionBO;
import com.tarkshala.scholars.engine.security.Role;
import com.tarkshala.scholars.engine.spring.SpringApplication;
import com.tarkshala.scholars.engine.webservices.filters.Authorization;
import com.tarkshala.scholars.engine.webservices.filters.PubliclyAllowed;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/account")
@Authorization
public class AccountService extends SpringApplication {

    private Logger logger = LogManager.getLogger(AccountService.class);

    @Path("/greet")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @PubliclyAllowed
    public Response greet(){
        logger.info("Welcome to Tarkshala Scholar Account APIs");
        return getBean(AuthenticationServiceHandler.class).greet();
    }

    @Path("/profile")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response getProfile(String username) {
        return getBean(AccountServiceHandler.class).getProfile(username);
    }

    @Path("/invoice")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response getInvoice(String scholarId) {
        return getBean(AccountServiceHandler.class).getInvoice(scholarId);
    }

    @Path("/transaction/create")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response createTransaction(TransactionBO transaction) {
        return getBean(AccountServiceHandler.class).createTransaction(transaction);
    }

    @Path("/transaction")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response getTransaction(String uid) {
        return getBean(AccountServiceHandler.class).getTransaction(uid);
    }
}
