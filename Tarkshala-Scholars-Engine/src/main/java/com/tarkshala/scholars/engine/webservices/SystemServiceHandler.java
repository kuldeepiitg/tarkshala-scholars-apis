package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.dao.EntityDAO;
import com.tarkshala.scholars.engine.dao.model.NotificationDO;
import com.tarkshala.scholars.engine.dao.model.StudentDO;
import com.tarkshala.scholars.engine.dao.model.TransactionDO;
import com.tarkshala.scholars.engine.exception.EngineBreakageException;
import com.tarkshala.scholars.engine.exception.EntityMissingException;
import com.tarkshala.scholars.engine.models.NotificationBO;
import com.tarkshala.scholars.engine.notification.EmailNotifier;
import com.tarkshala.scholars.engine.notification.SMSNotifier;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.*;

@Slf4j
@Component
public class SystemServiceHandler {

    @Autowired
    private EntityDAO<TransactionDO> transactionDAO;

    @Autowired
    private EmailNotifier emailNotifier;

    @Autowired
    private SMSNotifier smsNotifier;

    @Autowired
    private EntityDAO<NotificationDO> notificationDAO;

    @Autowired
    private EntityDAO<StudentDO> studentDAO;

    // TODO: 10/04/20 Reading all transactions is not very good idea, it can be designed better
    @SneakyThrows
    public Response notifyDodgers() {

        List<TransactionDO> transactions = transactionDAO.get(Arrays.asList());
        log.debug("Transaction have been fetched for all users");

        Map<String, Integer> debts = new HashMap<>();
        for (TransactionDO transaction: transactions) {
            if (debts.containsKey(transaction.getScholarUid())) {
                String scholarId = transaction.getScholarUid();
                int balance = debts.get(scholarId);
                if (transaction.getTransactionType() == TransactionDO.TransactionType.CREDIT) {
                    debts.put(scholarId, balance + transaction.getAmount());
                } else if (transaction.getDueDate() == null || transaction.getDueDate().before(new Date())){
                    debts.put(scholarId, balance - transaction.getAmount());
                }
            } else {
                if (transaction.getTransactionType() == TransactionDO.TransactionType.CREDIT) {
                    debts.put(transaction.getScholarUid(), transaction.getAmount());
                } else if (transaction.getDueDate() == null || transaction.getDueDate().before(new Date())){
                    debts.put(transaction.getScholarUid(), -transaction.getAmount());
                }
            }
        }
        log.debug("Debt have been calculated for each user");

        Optional<NotificationDO> notificationDO =
                notificationDAO.get(NotificationBO.Type.FINANCE_FEE_DODGER.name());
        if (notificationDO.isEmpty()) {
            log.error("Notification template of type {} is not available into system", NotificationBO.Type.FINANCE_FEE_DODGER);
            throw new EngineBreakageException("Notification template is not available for the given type.");
        }
        NotificationDO notification = notificationDO.get();
        for (Map.Entry<String, Integer> userDebt: debts.entrySet()) {
            if (userDebt.getValue() < 0) {
                // amount has to be positive, so multiplied by minus(-)
                notifyUser(notification, userDebt.getKey(), -(userDebt.getValue()));
            }
        }
        log.info("Users with due fee have been notified");
        return Response.ok("Notification have been sent to all fee dodgers").build();
    }

    private void notifyUser(NotificationDO notification, String studentId, int amount) {
        Optional<StudentDO> studentDO = studentDAO.get(studentId);
        if (studentDO.isEmpty()) {
            throw new EntityMissingException("Student record is missing with given id in system");
        }
        StudentDO student = studentDO.get();

        String subject = notification.getSubject().replace("$name", student.getName());
        String htmlBody = notification.getEmailTemplate().replace("$name", student.getName());
        String textBody = notification.getTextTemplate().replace("$name", student.getName());

        subject = subject.replace("$amount", Integer.toString(amount));
        htmlBody = htmlBody.replace("$amount", Integer.toString(amount));
        textBody = textBody.replace("$amount", Integer.toString(amount));

        emailNotifier.send(Constants.SYSTEM_EMAIL_ID ,student.getEmail(), subject, textBody, htmlBody);
        smsNotifier.send(student.getPhones().values(), subject, textBody);
    }
}
