package com.tarkshala.scholars.engine.crypto;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class PasswordHasher {

    public String hash(String password, String salt) {
        return Base64.getEncoder().encodeToString(DigestUtils.sha256(password + salt));
    }
}
