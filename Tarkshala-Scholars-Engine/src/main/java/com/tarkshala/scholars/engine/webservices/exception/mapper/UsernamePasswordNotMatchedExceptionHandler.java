package com.tarkshala.scholars.engine.webservices.exception.mapper;

import com.tarkshala.scholars.engine.exception.UsernamePasswordNotMatchedException;
import com.tarkshala.scholars.engine.models.LoginResponseBO;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UsernamePasswordNotMatchedExceptionHandler implements ExceptionMapper<UsernamePasswordNotMatchedException> {
    @Override
    public Response toResponse(UsernamePasswordNotMatchedException exception) {
        LoginResponseBO loginResponseBO = LoginResponseBO.builder()
                .status(0)
                .message(exception.getMessage())
                .build();
        return Response.status(Response.Status.UNAUTHORIZED).entity(loginResponseBO).build();
    }
}
