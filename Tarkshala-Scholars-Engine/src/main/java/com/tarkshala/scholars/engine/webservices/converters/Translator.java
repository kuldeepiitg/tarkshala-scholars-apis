package com.tarkshala.scholars.engine.webservices.converters;

import com.tarkshala.scholars.engine.dao.model.EntityDO;
import com.tarkshala.scholars.engine.models.EntityBO;

public interface Translator<T extends EntityDO, B extends EntityBO> {

    /**
     * Translate data object to business object.
     *
     * @param entityDO data object to be translated
     * @return the business object
     */
    B translate(T entityDO);

    T translate(B entityBO);
}
