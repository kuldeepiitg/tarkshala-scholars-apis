package com.tarkshala.scholars.engine.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.tarkshala.scholars.engine.dao.dynamodb.Filter;
import com.tarkshala.scholars.engine.dao.dynamodb.FilterExpression;
import com.tarkshala.scholars.engine.dao.model.TransactionDO;
import com.tarkshala.scholars.engine.webservices.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TransactionDAOImpl extends EntityDAO<TransactionDO> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public Optional<TransactionDO> get(String transactionId) {
        return Optional.ofNullable(dynamoDBMapper.load(TransactionDO.class, transactionId));
    }

    /**
     * Get all transactions related to given student
     *
     * @param studentId id for the student
     * @return list of all credit/debit transactions related to the student
     */
    public Optional<List<TransactionDO>> getAllForStudent(String studentId) {
        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":studentId", new AttributeValue().withS(studentId));
        DynamoDBQueryExpression<TransactionDO> queryExpression = new DynamoDBQueryExpression<TransactionDO>()
                .withKeyConditionExpression("scholarUid = :studentId")
                .withExpressionAttributeValues(eav)
                .withIndexName("scholarUid-entityCreationTime-index")
                .withConsistentRead(false);

        PaginatedQueryList<TransactionDO> transactionList = dynamoDBMapper.query(TransactionDO.class, queryExpression);
        return Optional.of(transactionList);
    }

    @Override
    public List<TransactionDO> get(List<Filter> filters, String previousPageLastKey, int count) {

        DynamoDBScanExpression scanExpression = super.getDynamoDBScanExpression(filters, previousPageLastKey, count);
        List<TransactionDO> transactions = new ArrayList<>();
        Iterator<TransactionDO> iterator = dynamoDBMapper.scan(TransactionDO.class, scanExpression).iterator();
        while (iterator.hasNext() && count > 0) {
            transactions.add(iterator.next());
            count--;
        }
        return transactions;
    }

    @Override
    public List<TransactionDO> get(List<Filter> filters) {
        return get(filters, null, Integer.MAX_VALUE);
    }
}
