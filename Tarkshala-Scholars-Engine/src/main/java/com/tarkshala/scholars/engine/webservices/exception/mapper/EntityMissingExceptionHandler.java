package com.tarkshala.scholars.engine.webservices.exception.mapper;

import com.tarkshala.scholars.engine.exception.EntityMissingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityMissingExceptionHandler implements ExceptionMapper<EntityMissingException> {
    @Override
    public Response toResponse(EntityMissingException exception) {
        return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
    }
}
