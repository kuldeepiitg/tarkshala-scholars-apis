package com.tarkshala.scholars.engine.security;

import com.tarkshala.scholars.engine.dao.EntityDAO;
import com.tarkshala.scholars.engine.dao.model.NotificationDO;
import com.tarkshala.scholars.engine.dao.model.PasswordResetDO;
import com.tarkshala.scholars.engine.dao.model.UserDO;
import com.tarkshala.scholars.engine.exception.EngineBreakageException;
import com.tarkshala.scholars.engine.exception.UserNotRegisteredException;
import com.tarkshala.scholars.engine.models.NotificationBO;
import com.tarkshala.scholars.engine.notification.EmailNotifier;
import com.tarkshala.scholars.engine.uid.UUIDGenerator;
import com.tarkshala.scholars.engine.webservices.Constants;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URL;
import java.util.Optional;

@Slf4j
@Component
public class PasswordResetLinkGenerator {

    @Autowired
    private EntityDAO<UserDO> userDAO;

    @Autowired
    private EntityDAO<PasswordResetDO> passwordResetDAO;

    @Autowired
    private UUIDGenerator uuidGenerator;

    @Autowired
    private EmailNotifier emailNotifier;

    @Autowired
    private EntityDAO<NotificationDO> notificationDAO;

    /**
     * Generate reset password object into system, so that reset
     * request can be identified. Also the reset link is sent to email account
     * of concerned user.
     *
     * @param username of concerned user
     */
    public void generateLink(String username) {

        log.debug("Generating reset password link for user {}", username);
        Optional<UserDO> user = userDAO.get(username);
        if (user.isEmpty()) {
            log.error("User with email id {} doesn't exists in system", username);
            throw new UserNotRegisteredException("User with this email id doesn't exists in system");
        }

        String uuid = uuidGenerator.generate();
        PasswordResetDO passwordResetDO = PasswordResetDO.builder()
                .userId(user.get().getEmail())
                .dead(false)
                .uid(uuid)
                .build();
        passwordResetDAO.save(passwordResetDO);
        sendEmail(user.get(), uuid);
    }

    private void sendEmail(UserDO user, String passwordResetUUid) {

        log.debug("Sending email with reset password link for user {}", user.getEmail());
        Optional<NotificationDO> notificationDO = notificationDAO.get(NotificationBO.Type.USER_ACCOUNT_ACTIVATION.name());
        if (notificationDO.isEmpty()) {
            log.error("Notification template is not available for {}", NotificationBO.Type.USER_ACCOUNT_ACTIVATION.name());
            throw new EngineBreakageException("Notification template is not available for the given type.");
        }

        NotificationDO notification = notificationDO.get();

        String subject = notification.getSubject().replace("$name", user.getName());
        String htmlBody = notification.getEmailTemplate().replace("$name", user.getName());
        String textBody = notification.getTextTemplate().replace("$name", user.getName());
        String url = createURL(passwordResetUUid);
        htmlBody = htmlBody.replace("$url", url);
        textBody = textBody.replace("$url", url);

        emailNotifier.send(Constants.SYSTEM_EMAIL_ID, user.getEmail(), subject, textBody, htmlBody);
    }

    @SneakyThrows
    private String createURL(String uuid){
        String protocol = "http";
        String host = "tarkshala.com";
        int port = 8080;
        String path = "/password/reset/" + uuid;
        String auth = null;
        String fragment = null;
        String query = null;
        URI uri = new URI(protocol, auth, host, port, path, query, fragment);
        URL url = uri.toURL();
        return url.toString();
    }

}
