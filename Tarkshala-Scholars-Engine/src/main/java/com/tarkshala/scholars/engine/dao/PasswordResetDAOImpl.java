package com.tarkshala.scholars.engine.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.tarkshala.scholars.engine.dao.model.PasswordResetDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PasswordResetDAOImpl extends EntityDAO<PasswordResetDO> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public Optional<PasswordResetDO> get(String uid) {
        return Optional.ofNullable(dynamoDBMapper.load(PasswordResetDO.class, uid));
    }
}
