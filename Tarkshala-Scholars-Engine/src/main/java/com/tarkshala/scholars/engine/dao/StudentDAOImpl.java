package com.tarkshala.scholars.engine.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.tarkshala.scholars.engine.dao.dynamodb.Filter;
import com.tarkshala.scholars.engine.dao.model.StudentDO;
import org.apache.http.MethodNotSupportedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StudentDAOImpl extends EntityDAO<StudentDO> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public Optional<StudentDO> get(String uid) {
        return Optional.ofNullable(dynamoDBMapper.load(StudentDO.class, uid));
    }

    @Override
    public List<StudentDO> get(List<Filter> filters) throws MethodNotSupportedException {
        return get(filters, null, Integer.MAX_VALUE);
    }

    @Override
    public List<StudentDO> get(List<Filter> filters, String previousPageLastKey, int count) throws MethodNotSupportedException {

        DynamoDBScanExpression scanExpression = super.getDynamoDBScanExpression(filters, previousPageLastKey, count);
        List<StudentDO> students = new ArrayList<>();
        Iterator<StudentDO> iterator = dynamoDBMapper.scan(StudentDO.class, scanExpression).iterator();
        while (iterator.hasNext() && count > 0) {
            students.add(iterator.next());
            count--;
        }
        return students;
    }
}
