package com.tarkshala.scholars.engine.webservices;

import com.tarkshala.scholars.engine.dao.dynamodb.Filter;
import com.tarkshala.scholars.engine.requests.StudentDirectoryPageRequest;
import com.tarkshala.scholars.engine.security.Role;
import com.tarkshala.scholars.engine.spring.SpringApplication;
import com.tarkshala.scholars.engine.webservices.filters.Authorization;
import com.tarkshala.scholars.engine.webservices.filters.Logged;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("/directory")
@Authorization
@Logged
public class DirectoryService extends SpringApplication {

    @Path("/list/students")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(Role.ADMINISTRATOR)
    public Response list(StudentDirectoryPageRequest request) {

        List<Filter> filters = new ArrayList<>();
        if (request.getFilters() != null) {
            filters = request.getFilters().stream().map(filter -> Filter.builder().attributeName(filter.getAttributeName())
                    .attributeValue(filter.getAttributeValue())
                    .attributeType(Filter.AttributeType.valueOf(filter.getAttributeType().name()))
                    .operation(Filter.Operation.valueOf(filter.getOperation().name()))
                    .build()).collect(Collectors.toList());
        }

        return getBean(DirectoryServiceHandler.class).list(request.getCount(), request.getLastEvaluatedKey(), filters);
    }
}
