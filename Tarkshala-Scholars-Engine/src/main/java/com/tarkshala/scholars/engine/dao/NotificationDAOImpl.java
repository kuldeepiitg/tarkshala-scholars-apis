package com.tarkshala.scholars.engine.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.tarkshala.scholars.engine.dao.model.NotificationDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class NotificationDAOImpl extends EntityDAO<NotificationDO> {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public Optional<NotificationDO> get(String type) {
        return Optional.ofNullable(dynamoDBMapper.load(NotificationDO.class, type));
    }
}
