package com.tarkshala.scholars.engine.dao.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@DynamoDBTable(tableName = "configuration-notification")
@DynamoDBDocument
public class NotificationDO extends EntityDO {

    /**
     * The UID field is deprecated as type will be the primary key for notification
     */
    @Deprecated
    private String uid;

    @DynamoDBHashKey
    private String type;

    @DynamoDBAttribute
    private String subject;

    @DynamoDBAttribute
    private String emailTemplate;

    @DynamoDBAttribute
    private String textTemplate;
}
